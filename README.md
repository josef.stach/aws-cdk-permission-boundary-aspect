# AWS CDK Permission Boundary Aspect

## Usage
```python
from permission_boundary import PermissionBoundaryAspect


my_stack.node.apply_aspect(
    PermissionBoundaryAspect(
        f'arn:aws:iam::{core.Aws.ACCOUNT_ID}:policy/my-permission-boundary'
    )
)
```
